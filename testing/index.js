const Generator = require('yeoman-generator');

module.exports = class GeneratorTesting extends Generator {
  writing() {
    // extend package.json
    this.fs.extendJSON(
      this.destinationPath('package.json'),
      this.fs.readJSON(this.templatePath('_package.json')),
    );
  }

  default() {
    this.composeWith(require.resolve('../testing-karma'), this.config.getAll());
  }
};
