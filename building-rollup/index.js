const Generator = require('yeoman-generator');

module.exports = class GeneratorBuildingRollup extends Generator {
  initializing() {
    if (Object.keys(this.config.getAll()).length === 0) {
      this.composeWith(require.resolve('../get-tag-name'), {
        __store: this.config,
      });
    }
  }

  writing() {
    // extend package.json
    this.fs.extendJSON(
      this.destinationPath('package.json'),
      this.fs.readJSON(this.templatePath('_package.json')),
    );

    // write & rename babel definition
    this.fs.copyTpl(
      this.templatePath('babel.es5.config.js'),
      this.destinationPath('babel.es5.config.js'),
      this.config.getAll(),
    );
    this.fs.copyTpl(
      this.templatePath('babel.es6.config.js'),
      this.destinationPath('babel.es6.config.js'),
      this.config.getAll(),
    );

    // write & rename rollup.dev definition
    this.fs.copyTpl(
      this.templatePath('rollup.config.dev.js'),
      this.destinationPath('rollup.config.dev.js'),
      this.config.getAll(),
    );

    // write & rename rollup.prod definition
    this.fs.copyTpl(
      this.templatePath('rollup.config.prod.js'),
      this.destinationPath('rollup.config.prod.js'),
      this.config.getAll(),
    );
  }
};
