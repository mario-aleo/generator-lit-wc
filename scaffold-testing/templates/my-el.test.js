import { html, fixture, expect } from '@open-wc/testing';

import '../src/<%= tagName %>';

describe('<<%= tagName %>>', () => {
  it('instantiating the element with default properties', async () => {
    const element = await fixture('<<%= tagName %>></<%= tagName %>>');
    const elementShadowRoot = element.shadowRoot;
    const elementHeader = elementShadowRoot.querySelector('h2');
    expect(element.prop1).to.equal('<%= tagName %>');
    expect(elementHeader.innerHTML).to.equal('Hello <%= tagName %>!');
  });

  it('setting a property on the element works', async () => {
    const element = await fixture(html`
      <<%= tagName %> prop1="new-prop1"></<%= tagName %>>
    `);
    const elementShadowRoot = element.shadowRoot;
    const elementHeader = elementShadowRoot.querySelector('h2');
    expect(element.prop1).to.equal('new-prop1');
    expect(elementHeader.innerHTML).to.equal('Hello new-prop1!');
  });
});
