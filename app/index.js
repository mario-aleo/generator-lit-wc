const Generator = require('yeoman-generator');

module.exports = class GeneratorApp extends Generator {
  initializing() {
    if (Object.keys(this.config.getAll()).length === 0) {
      this.composeWith(require.resolve('../get-tag-name'), {
        __store: this.config,
      });
    }
  }

  writing() {
    const { tagName } = this.config.getAll();

    // write package.json
    this.fs.copyTpl(
      this.templatePath('_package.json'),
      this.destinationPath('package.json'),
      this.config.getAll(),
    );

    // write & rename .gitignore
    this.fs.copy(this.templatePath('_gitignore'), this.destinationPath('.gitignore'));

    // write index.html
    this.fs.copyTpl(
      this.templatePath('src/index.html'),
      this.destinationPath(`src/index.html`),
      this.config.getAll(),
    );

    // write & rename element definition
    this.fs.copyTpl(
      this.templatePath('src/my-el.js'),
      this.destinationPath(`src/${tagName}.js`),
      this.config.getAll(),
    );

    // write everything else
    this.fs.copyTpl(
      this.templatePath('static/**/*'),
      this.destinationPath(),
      this.config.getAll(),
      undefined,
      { globOptions: { dot: true } },
    );
  }

  default() {
    this.composeWith(require.resolve('../linting'), this.config.getAll());
    this.composeWith(require.resolve('../building'), this.config.getAll());
    this.composeWith(require.resolve('../scaffold-testing'), this.config.getAll());
  }
};
