# \<<%= tagName %>>

This webcomponent follows the [open-wc](https://github.com/open-wc/open-wc) recommendation.

## Installation
```bash
npm i <%= tagName %>
```

## Usage
```html
<script type="module">
  import '<%= tagName %>/<%= tagName %>.js';
</script>

<<%= tagName %>></<%= tagName %>>
```

## Linting
```bash
npm run lint
```

## Formating
```bash
npm run format
```

## Testing using karma
```bash
npm run test
```

## Demoing
```bash
npm run start
```

## Building
```bash
npm run build
```