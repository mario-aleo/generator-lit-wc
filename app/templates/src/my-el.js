import { LitElement, html, css } from 'lit-element';

class <%= className %> extends LitElement {
  static get properties() {
    return {
      prop1: { type: String },
    };
  }

  static get styles() {
    return [
      css`
        :host {
          display: block;
        }
      `,
    ];
  }

  constructor() {
    super();
    this.prop1 = '<%= tagName %>';
  }

  render() {
    return html`
      <h2>Hello ${this.prop1}!</h2>
    `;
  }
}

window.customElements.define('<%= tagName %>', <%= className %>);
